---
title: "01_exploration"
output:
  html_notebook:
    toc: yes
    toc_float: true
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE) 
knitr::opts_knit$set(root.dir = 'midbrain_na/')
```

```{r}
getwd()
```
# Read data 
```{r}
df<-readRDS('/links/groups/treutlein/USERS/zhisong_he/Work/midbrain_organoid/data/seurat_HCL_processed.rds')
```

```{r}
library(patchwork)
library(Seurat)
library('ggplot2')
```


# Plot UMAP from Zhisong


```{r, fig.width=8, fig.height=8}

#UMAPPlot(df@reductions[["umap_harmony_rna"]]@cell.embeddings)+ NoLegend()
Idents(df) <- "annot_tent"
DimPlot(df, reduction = "umap_css_rna", label = T) +  guides(color = guide_legend(override.aes = list(size=2), ncol=1) )+ 
  theme(legend.text=element_text(size=rel(0.5)))
```
# Subset only RNA assay

```{r}
DefaultAssay(df) <- "RNA"
dfrna<-DietSeurat(df,  assays=c('RNA'), scale.data = TRUE) 
```

# Subset only Qian and up to 90 days
```{r}
dfrna<-subset(x= dfrna, subset = (protocol == "Qian" & age< 120))
```



```{r}
dfrna<-readRDS('data/rna_qian_90.rds')
```


```{r}
rm(df)
```


```{r, message=FALSE, results='hide'}
dfrna <- NormalizeData(dfrna)
dfrna<-FindVariableFeatures(dfrna, nfeatures = 3000)
```

```{r,message=FALSE, results='hide'}
dfrna <- ScaleData(dfrna , vars.to.regress = c("nFeature_RNA", "percent.mt"))
```

```{r, message=FALSE, results='hide'}
dfrna <- RunPCA(dfrna, npcs = 40)
```

```{r}
ElbowPlot(dfrna, ndims = ncol(Embeddings(dfrna, "pca")))
```


```{r, message=FALSE, results='hide'}
dfrna <- RunUMAP(dfrna, dims = 1:25)
```

```{r, message=FALSE, results='hide'}
dfrna <- FindNeighbors(dfrna, dims = 1:25)
dfrna <- FindClusters(dfrna, resolution = 1)
```



```{r}
DimPlot(dfrna, reduction = "umap", group.by="age")
```
```{r}
FeaturePlot(dfrna, c("OTX2", "TCF7L2"), ncol=2, pt.size = 0.1)

```

It is not clear, whether these differences come from age or from batch (sample coincide with batch)

```{r}
View(dfrna)
```


```{r}
saveRDS(dfrna, 'data/rna_qian_90.rds') 
```


# Try integration


```{r, message=FALSE, results='hide'}
library(simspec)

dfrna <- cluster_sim_spectrum(dfrna, label_tag = "age", cluster_resolution = 0.3)
dfrna <- RunUMAP(dfrna, reduction="css", dims = 1:ncol(Embeddings(dfrna, "css")))
#dfrna <- FindNeighbors(seurat, reduction = "css", dims = 1:ncol(Embeddings(seurat, "css"))) %>%
   # FindClusters(resolution = 0.6)

UMAPPlot(dfrna, group.by="age")
```
```{r}
library("ggsci")
```

Better colors
This looks much better - data is integrated

```{r}
UMAPPlot(dfrna, group.by="age")+scale_color_npg()
```
# Find resolution 
```{r,message=FALSE, results='hide'}
dfrna<-FindNeighbors(dfrna, reduction = "css", dims = 1:ncol(Embeddings(dfrna, "css")))
```


```{r}
library(gridExtra)


```

```{r, message=FALSE, results='hide'}
p <- list()
i= 1
for (res in c(0.1, 0.3, 0.5, 0.7, 1)){
  dfrna<-FindClusters(dfrna, resolution = res)
  p[[i]] <- DimPlot(dfrna, reduction = "umap", label = TRUE)+ggtitle(res) + theme(plot.title = element_text(hjust = 0.5))+ 
  theme(legend.text=element_text(size=rel(0.5)))
  i = i + 1

  
}


```

```{r, fig.width=10, fig.height=10}
do.call(grid.arrange,p)
```


```{r}
saveRDS(dfrna, 'data/rna_qian_90.rds') 
```


0.3 looks the best to me 



```{r, message=FALSE, results='hide'}
dfrna<-FindClusters(dfrna, resolution = 0.3)
```
```{r}
saveRDS(dfrna, 'data/rna_qian_90.rds') 
```

# Transferring labels from reference data
```{r}
dfrna<-readRDS('data/rna_qian_90.rds')
```


```{r}
ref<-readRDS('/links/groups/treutlein/USERS/zhisong_he/Work/midbrain_organoid/extdata/midbrain.seurat.rds')
View(ref)
```

```{r, fig.height = 7, fig.width = 7}
Idents(ref) <- "annot_tent"
DimPlot(ref, reduction = "umap_css", label = T) +  guides(color = guide_legend(override.aes = list(size=2), ncol=1) )+ 
  theme(legend.text=element_text(size=rel(0.5)))
```
https://satijalab.org/seurat/reference/dietseurat

```{r}
DefaultAssay(ref) <- "RNA"
```

```{r}
dim(dfrna)
```


```{r}
#fb.names <- rownames(dfrna[["RNA"]])

anchors <- FindTransferAnchors(reference = ref, query = dfrna)

predictions <- TransferData(anchorset = anchors, refdata = ref$annot_tent,
    dims = 1:20)
dfrna <- AddMetaData(dfrna, metadata = predictions)
```









```{r}
dfrna<-readRDS('data/rna_qian_90_labels.rds')
library(Seurat)
```
```{r}
library(ggplot2)
library("ggsci")
```


```{r}
Idents(dfrna) <- "predicted.id"
DimPlot(dfrna, reduction = "umap", group.by = "predicted.id", label = TRUE,
    label.size = 3, repel = TRUE) + ggtitle("Query transferred labels")
```



```{r, fig.height=8, fig.width=7}
FeaturePlot(dfrna, c("OTX2", "HOXB2", "TCF7L2" , "SHH", "SLC6A3"), ncol=2, pt.size = 0.1)
```







Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.


Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).

The preview shows you a rendered HTML copy of the contents of the editor. Consequently, unlike *Knit*, *Preview* does not run any R code chunks. Instead, the output of the chunk when it was last run in the editor is displayed.
