library(Seurat)
df<-readRDS('/links/groups/treutlein/USERS/zhisong_he/Work/midbrain_organoid/data/seurat_HCL_processed.rds')
DefaultAssay(df) <- "RNA"
dfrna<-DietSeurat(df,  assays=c('RNA'), scale.data = TRUE) 
dfrna<-subset(x= dfrna, subset = (protocol == "Qian" & age< 120))
saveRDS(dfrna, 'data/rna_qian_90.rds') 